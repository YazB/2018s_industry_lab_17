package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static ictgradschool.industry.testingandrefactoring.ex01.Robot.Direction.North;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;

        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testTurnNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn(); // E
        myRobot.turn(); // S
        myRobot.turn(); // W
        myRobot.turn(); // N

        assertEquals(Robot.Direction.North, myRobot.getDirection());
    }


    @Test
    public void testTurnWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn(); // E
        myRobot.turn(); // S
        myRobot.turn(); // W

        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }

    @Test
    public void testTurnSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn(); // E
        myRobot.turn(); // S

        assertEquals(Robot.Direction.South, myRobot.getDirection());
    }

    @Test
    public void testTurnEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn(); // E

        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }

    @Test
    public void testMoveNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Shouldn't be at the edge");
        }

        Assert.assertEquals(Robot.Direction.North, myRobot.getDirection()); // Direction shouldn;'t change with a move
        assertEquals(9, myRobot.row()); // Row should move by 1
        assertEquals(1, myRobot.column()); // Column shouldn't change as we are moving in the y-axis
    }

    @Test
    public void testMoveEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn(); // E

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Shouldn't be at the edge");
        }

        Assert.assertEquals(Robot.Direction.East, myRobot.getDirection()); // Direction shouldn;'t change with a move
        assertEquals(10, myRobot.row()); // Row should move by 1
        assertEquals(2, myRobot.column()); // Column shouldn't change as we are moving in the y-axis
    }

    @Test
    public void testMoveSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Shouldn't be at the edge");
        }

        Assert.assertEquals(Robot.Direction.North, myRobot.getDirection()); // Direction shouldn;'t change with a move
        assertEquals(9, myRobot.row()); // Row should move by 1
        assertEquals(1, myRobot.column()); // Column shouldn't change as we are moving in the y-axis

        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(9, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn();
        myRobot.turn();

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Shouldn't be at the edge");
        }


        Assert.assertEquals(Robot.Direction.South, myRobot.getDirection()); // Direction shouldn;'t change with a move
        assertEquals(10, myRobot.row()); // Row should move by 1
        assertEquals(1, myRobot.column()); // Column shouldn't change as we are moving in the y-axis
    }

    @Test
    public void testMoveWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn(); // E

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Shouldn't be at the edge");
        }

        Assert.assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(2, myRobot.column());

        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(2, myRobot.column());

        myRobot.turn(); // S
        myRobot.turn(); // W

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Shouldn't be at the edge");
        }


        Assert.assertEquals(Robot.Direction.West, myRobot.getDirection()); // Direction shouldn;'t change with a move
        assertEquals(10, myRobot.row()); // Row should move by 1
        assertEquals(1, myRobot.column()); // Column shouldn't change as we are moving in the y-axis
    }

    @Test
    public void testInvalidMoveNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            assertEquals(1, myRobot.row());
            assertEquals(1, myRobot.column());

        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testInvalidMoveEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn(); //Facing E

        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            assertEquals(10, myRobot.row());
            assertEquals(10, myRobot.column());

        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testInvalidMoveSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            assertEquals(1, myRobot.row());
            assertEquals(1, myRobot.column());

        } catch (IllegalMoveException e) {
            fail();
        }

        myRobot.turn();
        myRobot.turn();

        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            assertEquals(10, myRobot.row());
            assertEquals(1, myRobot.column());
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }
    @Test
    public void testInvalidMoveWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn(); //Facing E

        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            assertEquals(10, myRobot.row());
            assertEquals(10, myRobot.column());

        } catch (IllegalMoveException e) {
            fail();
        }

        myRobot.turn();
        myRobot.turn();

        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            assertEquals(10, myRobot.row());
            assertEquals(1, myRobot.column());
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }
}


