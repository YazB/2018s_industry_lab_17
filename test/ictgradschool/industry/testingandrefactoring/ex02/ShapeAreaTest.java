package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShapeAreaTest {

    private ShapeAreaCalculator myShape;
    private Double width;
    private Double length;

    @Before
    public void setUp() {

        this.width =20.5;
        this.length = 18.5;
        myShape = new ShapeAreaCalculator(width, length);
    }


    @Test
    public void testStringToDouble() {
        assertEquals(this.string, myShape.getLengthString());
        Double d = Double.parseDouble(s);
        assertEquals(this.string, myShape.getWidthString());
        Double e = Double.parseDouble(t);
    }


    @Test
    public void testRectangleArea() {
        assertEquals(this.width, myShape.getWidth());
        assertEquals (this.length, myShape.getLength());
        //Don't like this optional of nullable thing!
        myShape.getRectangleArea(width, length);
    }

    @Test
    public void testCircleArea() {
        assertEquals(this.circleArea, myShape.getCircleArea);
    }

    @Test
    public void testRounding() {
        Math.round(myShape.getRectangleArea());
        Math.round(myShape.getCircleArea());

    }

    @Test
    public void testSmallerArea() {
        assertTrue((getCircleArea())> getRecentangleArea());
    }
}

