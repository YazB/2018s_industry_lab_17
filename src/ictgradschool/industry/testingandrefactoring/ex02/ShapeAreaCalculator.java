package ictgradschool.industry.testingandrefactoring.ex02;

import ictgradschool.Keyboard;


public class ShapeAreaCalculator {

    private String string;
    private Double width;
    private Double length;
    private Double RectangleArea;


    public ShapeAreaCalculator() {
        System.out.println("Welcome to Shape Area Calculator!");
        System.out.println("Enter the width of the rectangle: ");
        System.out.println("Enter the length of the rectangle: ");
        String input = Keyboard.readInput();
    }



    public Double convertToDouble(String value){
        return Double.parseDouble(value);
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getRectangleArea(Double width, Double length) {
       double rArea = this.width*this.length;
        return rArea;
    }
}
